package com.criscor.market.persistance.crud;

import com.criscor.market.persistance.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteCrudRepository extends CrudRepository<Cliente,String> {
}
