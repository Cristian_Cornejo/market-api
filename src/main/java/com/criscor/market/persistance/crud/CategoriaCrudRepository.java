package com.criscor.market.persistance.crud;

import com.criscor.market.persistance.entity.Categoria;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaCrudRepository extends CrudRepository<Categoria,Integer> {
}
