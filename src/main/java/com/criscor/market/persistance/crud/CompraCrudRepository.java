package com.criscor.market.persistance.crud;

import com.criscor.market.persistance.entity.Compra;
import org.springframework.data.repository.CrudRepository;

public interface CompraCrudRepository extends CrudRepository<Compra,Integer> {
}
