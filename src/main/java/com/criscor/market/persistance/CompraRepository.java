package com.criscor.market.persistance;

import com.criscor.market.domain.Purchase;
import com.criscor.market.domain.repository.PurchaseRepository;
import com.criscor.market.persistance.crud.CompraCrudRepository;
import com.criscor.market.persistance.entity.Compra;
import com.criscor.market.persistance.mapper.PurchaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public class CompraRepository implements PurchaseRepository {

    @Autowired
    private PurchaseMapper mapper;

    @Autowired private CompraCrudRepository repository;


    @Override
    public List<Purchase> getAll() {
        return mapper.toPurchases((List<Compra>) repository.findAll());
    }

    @Override
    public Optional<Purchase> getByPurchase(int purchaseId) {
        return repository.findById(purchaseId).map(compra -> mapper.toPurchase(compra));
    }

    @Override
    public Purchase savePurchase(Purchase purchase) {
        return mapper.toPurchase(repository.save(mapper.toCompra(purchase)));
    }

    @Override
    public void deletePurchase(int purchaseId) {
        repository.deleteById(purchaseId);
    }
}
