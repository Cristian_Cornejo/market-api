package com.criscor.market.persistance;

import com.criscor.market.domain.Category;
import com.criscor.market.domain.repository.CategoryRepository;
import com.criscor.market.persistance.crud.CategoriaCrudRepository;
import com.criscor.market.persistance.entity.Categoria;
import com.criscor.market.persistance.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoriaRepository implements CategoryRepository {

    @Autowired
    private CategoriaCrudRepository categoriaCrudRepository;

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<Category> getAll() {
        return categoryMapper.toCategories((List<Categoria>) categoriaCrudRepository.findAll());
    }

    @Override
    public Optional<Category> getByCategory(int categoryId) {
        return categoriaCrudRepository.findById(categoryId).map(categoria -> categoryMapper.toCategory(categoria));
    }

    @Override
    public Category saveCategory(Category category) {
        return categoryMapper.toCategory(categoriaCrudRepository.save(categoryMapper.toCategoria(category)));
    }

    @Override
    public void deleteCategory(int categoryId) {
        categoriaCrudRepository.deleteById(categoryId);
    }
}
