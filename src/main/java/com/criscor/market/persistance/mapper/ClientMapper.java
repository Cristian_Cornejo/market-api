package com.criscor.market.persistance.mapper;

import com.criscor.market.domain.Client;
import com.criscor.market.persistance.entity.Cliente;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = {PurchaseMapper.class})
public interface ClientMapper {

    @Mappings({
            @Mapping(source = "id",target = "clientId"),
            @Mapping(source = "nombre",target = "firstname"),
            @Mapping(source = "apellidos",target = "lastname"),
            @Mapping(source = "celular",target = "cellphone"),
            @Mapping(source = "direccion",target = "address"),
            @Mapping(source = "correoElectronico",target = "email"),
            @Mapping(source = "compras",target = "purchases")
    })
    Client toClient(Cliente cliente);
    List<Client> toClients(List<Cliente> clientes);

    @InheritInverseConfiguration
    Cliente toCliente(Client client);
    List<Cliente> toClientes(List<Client> clients);
}
