package com.criscor.market.persistance;

import com.criscor.market.domain.Client;
import com.criscor.market.domain.repository.ClientRepository;
import com.criscor.market.persistance.crud.ClienteCrudRepository;
import com.criscor.market.persistance.entity.Cliente;
import com.criscor.market.persistance.mapper.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ClienteRepository implements ClientRepository {
    @Autowired
    private ClienteCrudRepository repository;

    @Autowired
    private ClientMapper mapper;


    @Override
    public List<Client> getAll() {
        return mapper.toClients((List<Cliente>) repository.findAll());
    }

    @Override
    public Optional<Client> getByClient(String clientId) {
        return repository.findById(clientId).map(cliente -> mapper.toClient(cliente));
    }

    @Override
    public Client saveClient(Client client) {

        return mapper.toClient(repository.save(mapper.toCliente(client)));
    }

    @Override
    public void deleteClient(String clientId) {
        repository.deleteById(clientId);
    }
}
