package com.criscor.market.persistance;

import com.criscor.market.domain.Product;
import com.criscor.market.domain.repository.ProductRepository;
import com.criscor.market.persistance.crud.ProductoCrudRepository;
import com.criscor.market.persistance.entity.Producto;
import com.criscor.market.persistance.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductoRepository implements ProductRepository {
    @Autowired
    private ProductoCrudRepository productoCrudRepository;
    @Autowired
    private ProductMapper mapper;

    @Override
    public List<Product> getAll() {
        return mapper.toProducts((List<Producto>) productoCrudRepository.findAll());
    }

    @Override
    public Optional<List<Product>> getByCategory(int categoryId) {
        List<Producto> productos = productoCrudRepository.findByIdCategoriaOrderByNombreAsc(categoryId);
        return Optional.of(mapper.toProducts(productos));
    }

    @Override
    public Optional<List<Product>> getScarseProducts(int quantity) {
        return productoCrudRepository
                .findByCantidadStockLessThanAndEstado(quantity,true).map(prods-> mapper.toProducts(prods));
    }

    @Override
    public Optional<Product> getProduct(int productId) {
        return productoCrudRepository.findById(productId).map(producto -> mapper.toProdut(producto));
    }

    @Override
    public Product saveProduct(Product product) {
        return mapper.toProdut(productoCrudRepository.save(mapper.toProducto(product)));
    }

    @Override
    public void deleteProduct(int productId) {
        productoCrudRepository.deleteById(productId);
    }
}
