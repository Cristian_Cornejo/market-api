package com.criscor.market;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CriscorMarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(CriscorMarketApplication.class, args);
	}

}
