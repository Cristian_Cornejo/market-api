package com.criscor.market.domain.service;

import com.criscor.market.domain.Category;
import com.criscor.market.domain.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> getAll(){
        return categoryRepository.getAll();
    }

    public Optional<Category> getByCategory(int categoryId){
        return categoryRepository.getByCategory(categoryId);
    }

    public Category saveCategory(Category category){
        return  categoryRepository.saveCategory(category);
    }

    public Boolean deleteCategory(int categoryId){
        return  categoryRepository.getByCategory(categoryId).map(category -> {
            categoryRepository.deleteCategory(categoryId);
            return true;
        }).orElse(false);

    }
}
