package com.criscor.market.domain.service;

import com.criscor.market.domain.Client;
import com.criscor.market.domain.repository.ClientRepository;
import com.criscor.market.persistance.entity.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository repository;

    public List<Client> getAll() {
        return repository.getAll();
    }

    public Optional<Client> getByClient(String clientId) {
        return repository.getByClient(clientId);
    }

    public Client saveClient(Client client) {
        return repository.saveClient(client);
    }

    public Boolean deleteClient(String clientId) {
       return repository.getByClient(clientId).map(client -> {
            repository.deleteClient(clientId);
            return true;
        }).orElse(false);
    }
}
