package com.criscor.market.domain.service;

import com.criscor.market.domain.Purchase;
import com.criscor.market.domain.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    private PurchaseRepository repository;

    public List<Purchase> getAll() {
        return repository.getAll();
    }

    public Optional<Purchase> getByPurchase(int purchaseId) {
        return repository.getByPurchase(purchaseId);
    }

    public Purchase savePurchase(Purchase purchase) {
        return repository.savePurchase(purchase);
    }

    public Boolean deletePurchase(int purchaseId) {
       return repository.getByPurchase(purchaseId).map(purchase -> {
            repository.deletePurchase(purchaseId);
            return true;
        }).orElse(false);
    }
}
