package com.criscor.market.domain.repository;

import com.criscor.market.domain.Purchase;

import java.util.List;
import java.util.Optional;

public interface PurchaseRepository {
    List<Purchase> getAll();
    Optional<Purchase> getByPurchase(int purchaseId);
    Purchase savePurchase(Purchase purchase);
    void deletePurchase(int purchaseId);
}
