package com.criscor.market.domain.repository;

import com.criscor.market.domain.Client;

import java.util.List;
import java.util.Optional;

public interface ClientRepository {
    List<Client> getAll();
    Optional<Client> getByClient(String clientId);
    Client saveClient(Client client);
    void deleteClient(String clientId);
}
