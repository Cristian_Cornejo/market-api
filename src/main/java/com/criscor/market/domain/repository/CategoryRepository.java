package com.criscor.market.domain.repository;

import com.criscor.market.domain.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {
    List<Category> getAll();
    Optional<Category> getByCategory(int categoryId);
    Category saveCategory(Category category);
    void deleteCategory(int categoryId);
}
