package com.criscor.market.web.controller;

import com.criscor.market.domain.Category;
import com.criscor.market.domain.service.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    @ApiOperation("Obtiene una lsita con toda las categorias que puede tener un producto")
    public ResponseEntity<List<Category>> getAll(){
        return new ResponseEntity<>(categoryService.getAll(), HttpStatus.OK);
    }

    @ApiOperation("Obtiene una categoria a traves de su id")
    @GetMapping("/{categoryId}")
    public ResponseEntity<Category> getByCategory(
            @ApiParam(value = "id de la categoría",required = true,example = "1")
            @PathVariable int categoryId){
       return categoryService.getByCategory(categoryId).map(category ->
                new ResponseEntity(category,HttpStatus.OK)
        ).orElse(new ResponseEntity(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ApiOperation("Crea una nueva categoria")
    public ResponseEntity<Category> saveCategory(@RequestBody Category category){
        return new ResponseEntity<>(categoryService.saveCategory(category),HttpStatus.CREATED);
    }

    @DeleteMapping("/{categoryId}")
    @ApiOperation("Elimina una categoria buscandola por su id")
    public ResponseEntity deleteCategory(
            @ApiParam(value = "id de la categoría",required = true,example = "1")
            @PathVariable int categoryId){
        if(categoryService.deleteCategory(categoryId)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
