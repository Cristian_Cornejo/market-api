package com.criscor.market.web.controller;

import com.criscor.market.domain.Product;
import com.criscor.market.domain.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductService productService;
    @GetMapping
    @ApiOperation("Obtiene todos los productos")
    public ResponseEntity<List<Product>> getAll(){

        return new ResponseEntity<>(productService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{productId}")
    @ApiOperation("Obtiene un producto buscandolo por su id")
    public ResponseEntity<Product> getProduct(
            @ApiParam(value = "id del producto",required = true,example = "1")
            @PathVariable int productId){
        return productService.getProduct(productId)
                .map(product -> new ResponseEntity<>(product,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/category/{categoryId}")
    @ApiOperation("Obtiene los productos pertenecientes a la categoria")
    public ResponseEntity<List<Product>> getProductByCategory(
            @ApiParam(value = "id de la categoria",required = true,example = "1")
            @PathVariable int categoryId){
        return productService.getProductByCategory(categoryId)
                .map(products -> new ResponseEntity<>(products,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/scarse/{quantity}")
    @ApiOperation("Obtiene los producctos activos que tengan un stock menor al enviado por parametro")
    public ResponseEntity<List<Product>> getScarseProducts(
            @ApiParam(value = "cantidad",required = true,example = "20")
            @PathVariable int quantity){
        return productService.getScarseProducts(quantity)
                .map(products -> new ResponseEntity<>(products,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ApiOperation("Crea un nuevo producto")
    public ResponseEntity<Product> saveProduct(@RequestBody Product product){
        return new ResponseEntity<>(productService.saveProduct(product),HttpStatus.CREATED);
    }

    @ApiOperation("Elimina el producto obteniendolo por su id")
    @DeleteMapping("/{productId}")
    public ResponseEntity deleteProduct(
            @ApiParam(value = "id de la categoria",required = true,example = "1")
            @PathVariable int productId){
       if( productService.deleteProduct(productId)){
           return new ResponseEntity(HttpStatus.OK);
       }else{
           return new ResponseEntity(HttpStatus.NOT_FOUND);
       }
    }
}
