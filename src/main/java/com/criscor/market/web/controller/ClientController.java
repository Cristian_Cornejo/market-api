package com.criscor.market.web.controller;

import com.criscor.market.domain.Client;
import com.criscor.market.domain.service.ClientService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("clients")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping
    @ApiOperation("Obtiene una lista con todos los clientes")
    public ResponseEntity<List<Client>> getAll() {
        return new ResponseEntity<>(clientService.getAll(),HttpStatus.OK);
    }
    @GetMapping("/{clientId}")
    @ApiOperation("Obtiene un cliente por su id")
    public ResponseEntity<Client> getByClient(
            @ApiParam(value = "id del cliente",required = true,example = "1")
            @PathVariable String clientId) {
        return clientService.getByClient(clientId)
                .map(client -> new ResponseEntity<>(client,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    @PostMapping()
    @ApiOperation("Crea un nuevo cliente")
    public ResponseEntity<Client> saveClient(@RequestBody Client client) {
        return new ResponseEntity<>(clientService.saveClient(client),HttpStatus.CREATED);
    }
    @DeleteMapping("/{clientId}")
    @ApiOperation("Elimina un cliente buscandolo por su id")
    public ResponseEntity deleteClient(
            @ApiParam(value = "id del cliente",required = true,example = "1")
            @PathVariable String clientId) {
      if(clientService.deleteClient(clientId)){
          return  new ResponseEntity(HttpStatus.OK);
      }else {
          return new ResponseEntity(HttpStatus.NOT_FOUND);
      }
    }
}
