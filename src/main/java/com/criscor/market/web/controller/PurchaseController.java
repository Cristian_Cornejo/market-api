package com.criscor.market.web.controller;

import com.criscor.market.domain.Purchase;
import com.criscor.market.domain.service.PurchaseService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("purchases")
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @GetMapping
    @ApiOperation("Obtiene una lista con todas las compras")
    public ResponseEntity<List<Purchase>> getAll() {
        return new ResponseEntity<>(purchaseService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{purchaseId}")
    @ApiOperation("Obtiene una compra buscandola por su id")
    public ResponseEntity<Purchase> getByPurchase(
            @ApiParam(value = "id de la compra",required = true,example = "1")
            @PathVariable int purchaseId) {
        return  purchaseService.getByPurchase(purchaseId)
                .map(purchase -> new ResponseEntity<>(purchase,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ApiOperation("Crea una nueva compra")
    public ResponseEntity<Purchase> savePurchase(@RequestBody Purchase purchase) {
        return new ResponseEntity<>(purchaseService.savePurchase(purchase),HttpStatus.CREATED);
    }

    @DeleteMapping("/{purchaseId}")
    @ApiOperation("Elimina una compra buscandola por su id")
    public ResponseEntity deletePurchase(
            @ApiParam(value = "id de la compra",required = true,example = "1")
            @PathVariable int purchaseId) {
        if(purchaseService.deletePurchase(purchaseId)){
            return  new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
